import {Router} from 'express';
import UserController from "./controllers/UserController";
import DocumentController from "./controllers/DocumentController";
import MusicController from "./controllers/MusicController";
import MovieController from "./controllers/MovieController";
import BookController from "./controllers/BookController";
import GenreController from "./controllers/GenreController";
import BorrowController from "./controllers/BorrowController";
import CommentController from "./controllers/CommentController";
import Auth from "./utils/Auth";
import Multer from "./utils/Multer";

const router = Router();

/**
 * Users
 */
router.get('/users', Auth.isAllowed([10]), UserController.list);
router.get('/users/:id', UserController.details);
router.post('/users/auth', UserController.auth);
router.post('/users',  UserController.store);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.remove);
router.put('/users/:id/picture', Multer.upload('users', 'picture'), UserController.updatePicture);

/**
 * @Document
 */
router.get('/documents',DocumentController.list);
router.get('/documents/:id', DocumentController.details);
router.post('/documents',  DocumentController.store);
router.put('/documents/:id', DocumentController.update);
router.delete('/documents/:id', DocumentController.remove);
router.put('/documents/:id/picture', Multer.upload('documents', 'picture'), DocumentController.updatePicture);
/**
 * @Music
 */
router.get('/musics',MusicController.list);
router.get('/musics/:id', MusicController.details);
router.post('/musics',  MusicController.store);
router.put('/musics/:id',MusicController.update);
router.delete('/musics/:id', MusicController.remove);
/**
 * @Movie
 */
router.get('/movies',MovieController.list);
router.get('/movies/:id', MovieController.details);
router.post('/movies',  MovieController.store);
router.put('/movies/:id',MovieController.update);
router.delete('/movies/:id', MovieController.remove);
/**
 * @Book
 */
router.get('/books',BookController.list);
router.get('/books/:id', BookController.details);
router.post('/books',  BookController.store);
router.put('/books/:id',BookController.update);
router.delete('/books/:id', BookController.remove);
/**
 * @Genre
 */
router.get('/genres',GenreController.list);
router.get('/genres/:id', GenreController.details);
router.post('/genres',  GenreController.store);
router.put('/genres/:id',GenreController.update);
router.delete('/genres/:id', GenreController.remove);
/**
 * @Borrow
 */
router.get('/borrows',BorrowController.list);
router.get('/borrows/:id',BorrowController.details);
router.post('/borrows',BorrowController.store);
router.put('/borrows/:id',BorrowController.update);
router.delete('/borrows/:id',BorrowController.remove);
/**
 * @Comment
 */
router.get('/comments',CommentController.list);
router.get('/comments/:id',CommentController.details);
router.post('/comments',CommentController.store);
router.put('/comments/:id',CommentController.update);
router.delete('/comments/:id',CommentController.remove);
/**
 * @test
 */
router.get('/test', (req, res) => {
 res.send({response: 'Je suis une réponse'});

});

export default router;