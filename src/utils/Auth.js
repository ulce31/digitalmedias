import jsonwebtoken from 'jsonwebtoken';
import User from '../models/User';
import crypto from 'crypto';

const algorithm = 'aes-256-cbc';
const key = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);
export default class Auth{

    /**
     * Check if user is right to access routes
     * @param roles
     * @returns {function(...[*]=)}
     */
 
    static isAllowed(roles){
        return async (req, res, next) => {

            try{
                let token = req.headers.authorization.replace(/Bearer /g, '');
                let decryptToken = jsonwebtoken.decode(token, process.env.JWT_SECRET);
                let user = await User.findById(decryptToken.sub);

                // if(roles.includes(user.role)){
                //     next();
                // }else{
                //     return res.status(401).json({message: 'Unauthorized'});
                // }
            }catch (e) {
                return res.status(403).json({message: 'Missing token'});
            }
        }
    }

    static encrypt(text) {
        let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
        let encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return  encrypted.toString('hex') ;
       }
     static decrypt(text) {
        let iv = Buffer.from(text.iv, 'hex');
        let encryptedText = Buffer.from(text.encryptedData, 'hex');
        let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
       }
}
