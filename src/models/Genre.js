import { Schema, model } from 'mongoose';


const GenreSchema = new Schema({
    title: {type: String, required: true},
    description: {type: String},
    created_at: {type: Date, default: Date.now()},
});

export default model( 'Genre', GenreSchema);
