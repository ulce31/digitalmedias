import { Schema, model } from 'mongoose';

const UserSchema = new Schema({
    firstname: {type: String, required: true},
    lastname: {type: String, required: true},
    email: {type: String, unique: true, required: true},
    password: {type: String, required: true},
    picture:{type:String},
    country: {type: String},
    city: {type: String},
    postal: {type: String},
    phone: {type: String},
    role: {type: Number, default: 0},
    created_at: {type: Date, default: Date.now()},
});

export default model('User', UserSchema);
