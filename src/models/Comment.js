import { Schema, model } from 'mongoose';

const CommentSchema = new Schema({
    borrow : {type: Schema.Types.ObjectId, required: true,ref: 'Borrow'},
    comment: {type: String},
    note: {type: String},
    created_at: {type: Date, default: Date.now()},
});

export default model('Comment', CommentSchema);