import { Schema, model } from 'mongoose';

import DocumentSchema from "../models/Document";

const extendSchema = require('mongoose-extend-schema');

const BookSchema = extendSchema(DocumentSchema,{  
    document: {type: Schema.Types.ObjectId, required: true,ref: 'Document'},
    nbPage: {type: String},
    description: {type: String},
    age:{type: String},
});

export default model('Book', BookSchema);
