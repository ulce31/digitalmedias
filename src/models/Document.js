import { Schema, model } from 'mongoose';

const DocumentSchema = new Schema({
    title: {type: String},
    picture:{type:String},
    genre: {type: Schema.Types.ObjectId,ref: 'Genre'},
    author: {type: String},
    date: {type: String},   
    credits: {type: String},
    type:{type: String, required: true},
    created_at: {type: Date, default: Date.now()},
});

export default model('Document', DocumentSchema);
