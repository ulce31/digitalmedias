import { Schema, model } from 'mongoose';

const BorrowSchema = new Schema({
    user: {type: Schema.Types.ObjectId, required: true,ref: 'User'},
    document: {type: Schema.Types.ObjectId, required: true,ref: 'Genre'},
    created_at: {type: Date, default: Date.now()},
});

export default model('Borrow', BorrowSchema);