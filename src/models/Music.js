import { Schema, model } from 'mongoose';

import DocumentSchema from "../models/Document";

const extendSchema = require('mongoose-extend-schema');

const MusicSchema = extendSchema(DocumentSchema,{  
    document: {type: Schema.Types.ObjectId, required: true,ref: 'Document'},
    duration: {type: String},
    age_restriction:{type: String},
});

export default model('Music', MusicSchema);
