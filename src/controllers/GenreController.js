import Genre from "../models/Genre";

/*import Utils from "../utils/Utils";*/

export default class GenreController{


    /**
     * Get list of Genres
     * @param req
     * @param res
     * @returns {Promise<any>}
     * 
     * @swagger
     * /genres:
     *   get:
     *     tags:
     *       - Genres
     *     name: List of genres
     *     summary: List of genres
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     * 
     */
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            //Récupération utilisateurs
            let genre = await Genre.find().select('-__v');
            body = {genre};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Genre list',
                message: e.message || 'An error is occured into Genre list',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Get details of Genre
     * @param req
     * @param res
     * @returns {Promise<any>}
     * 
     *@swagger
     * /genres/{id}:
     *   get:
     *     tags:
     *       - Genres
     *     name: get one genres
     *     summary: get one genres
     *     parameters:
     *        - in: path
     *          name: id
     *          type: string
     *          required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let genre = await Genre.findById(id).select('-__v');
            body = {genre};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Genre details',
                message: e.message || 'An error is occured into Genre details',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Create one Genre
     * @param req
     * @param res
     * @returns {Promise<any>}
     * 
     * @swagger
     * /genres:
     *   post:
     *     tags:
     *       - Genres
     *     name: Create one genres
     *     summary: Create one genres
     *     parameters:
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async store(req, res){
        let status = 200;
        let body = {};
    

        try{
            let genre = await Genre.create(req.body);
            body={genre};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Genre create',
                message: e.message || 'An error is occured into Genre create',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Update one Genre
     * @param req
     * @param res
     * @returns {Promise<any>}
     * 
     * @swagger
     * /genres/{id}:
     *   put:
     *     tags:
     *       - Genres
     *     name: Update one genres
     *     summary: Update one genres
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let genre = await Genre.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {genre};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Genre update',
                message: e.message || 'An error is occured into Genre update',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Remove one Genre
     * @param req
     * @param res
     * @returns {Promise<any>}
     * 
     * @swagger
     * 
     * /genres/{id}:
     *   delete:
     *     tags:
     *       - Genres
     *     name: Delete one genres
     *     summary: Delete one genres
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Genre.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Genre remove',
                message: e.message || 'An error is occured into Genre remove',
            }
        }
        return res.status(status).json(body);
    }
}
