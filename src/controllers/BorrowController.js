import Borrow from "../models/Borrow";

/*import Utils from "../utils/Utils";*/

export default class BorrowController{


    /**
     * Get list of Borrows
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /borrows:
     *   get:
     *     tags:
     *       - Borrows
     *     name: List of borrows
     *     summary: List of borrows
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     * 
     */
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            //Récupération utilisateurs
            let borrows = await Borrow.find().select('-__v');
            body = {borrows};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Borrow list',
                message: e.message || 'An error is occured into Borrow list',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Get details of Borrow
     * @param req
     * @param res
     * @returns {Promise<any>}
     *@swagger
     * /borrows/{id}:
     *   get:
     *     tags:
     *       - Borrows
     *     name: get one borrows
     *     summary: get one borrows
     *     parameters:
     *        - in: path
     *          name: id
     *          type: string
     *          required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let borrow = await Borrow.findById(id).select('-__v');
            body = {borrow};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Borrow details',
                message: e.message || 'An error is occured into Borrow details',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Create one Borrow
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /borrows:
     *   post:
     *     tags:
     *       - Borrows
     *     name: Create one borrows
     *     summary: Create one borrows
     *     parameters:
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async store(req, res){
        let status = 200;
        let body = {};
    

        try{
            let borrow = await Borrow.create(req.body);
            body={borrow};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Borrow create',
                message: e.message || 'An error is occured into Borrow create',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Update one Borrow
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /borrows/{id}:
     *   put:
     *     tags:
     *       - Borrows
     *     name: Update one borrows
     *     summary: Update one borrows
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let borrow = await Borrow.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {borrow};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Borrow update',
                message: e.message || 'An error is occured into Borrow update',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Remove one Borrow
     * @param req
     * @param res
     * @returns {Promise<any>}
      * @swagger
     * 
     * /borrows/{id}:
     *   delete:
     *     tags:
     *       - Borrows
     *     name: Delete one borrows
     *     summary: Delete one borrows
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Borrow.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Borrow remove',
                message: e.message || 'An error is occured into Borrow remove',
            }
        }
        return res.status(status).json(body);
    }
}
