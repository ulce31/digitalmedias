import Comment from "../models/Comment";

/*import Utils from "../utils/Utils";*/

export default class CommentController{


    /**
     * Get list of Comments
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /comments:
     *   get:
     *     tags:
     *       - Comments
     *     name: List of comments
     *     summary: List of comments
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     * 
     */
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            //Récupération utilisateurs
            let comments = await Comment.find().select('-__v');
            body = {comments};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Comment list',
                message: e.message || 'An error is occured into Comment list',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Get details of Comment
     * @param req
     * @param res
     * @returns {Promise<any>}
     *@swagger
     * /comments/{id}:
     *   get:
     *     tags:
     *       - Comments
     *     name: get one comments
     *     summary: get one comments
     *     parameters:
     *        - in: path
     *          name: id
     *          type: string
     *          required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let comment = await Comment.findById(id).select('-__v');
            body = {comment};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Comment details',
                message: e.message || 'An error is occured into Comment details',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Create one Comment
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /comments:
     *   post:
     *     tags:
     *       - Comments
     *     name: Create one comments
     *     summary: Create one comments
     *     parameters:
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async store(req, res){
        let status = 200;
        let body = {};
    

        try{
            let comment = await Comment.create(req.body);
            body={comment};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Comment create',
                message: e.message || 'An error is occured into Comment create',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Update one Comment
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /comments/{id}:
     *   put:
     *     tags:
     *       - Comments
     *     name: Update one comments
     *     summary: Update one comments
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let comment = await Comment.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {comment};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Comment update',
                message: e.message || 'An error is occured into Comment update',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Remove one Comment
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * 
     * /comments/{id}:
     *   delete:
     *     tags:
     *       - Comments
     *     name: Delete one comments
     *     summary: Delete one comments
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Comment.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Comment remove',
                message: e.message || 'An error is occured into Comment remove',
            }
        }
        return res.status(status).json(body);
    }
}
