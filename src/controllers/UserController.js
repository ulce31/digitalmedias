import User from "../models/User";
import jsonwebtoken from 'jsonwebtoken';
import fs from 'fs';
import Auth from "../utils/Auth";
/*import Utils from "../utils/Utils";*/
export default class UserController{

    static async auth(req, res){
        let status = 200;
        let body = {};

        try{
            let {email, password} = req.body;
          console.log(Auth.encrypt("amina   A2E324"));
            let user = await User.findOne({'email': email}).select('-__v');
            if(user && password === user.password){
                //L'utilisateur existe dans la base de données
                let {JWT_SECRET} = process.env;

                let token = jsonwebtoken.sign({sub: user._id}, JWT_SECRET);
                body = {user, token};
            }else{
                status = 401;
                new Error('Unauthorized');
            }
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User authentication',
                message: e.message || 'An error is occured into user auth',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Get list of users
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /user:
     *   get:
     *     tags:
     *       - User
     *     name: List of user
     *     summary: List of user
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     * 
     */
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            //Récupération utilisateurs
            let users = await User.find().select('-__v -password');
            body = {users};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User list',
                message: e.message || 'An error is occured into user list',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Get details of user
     * @param req
     * @param res
     * @returns {Promise<any>}
     *@swagger
     * /user/{id}:
     *   get:
     *     tags:
     *       - User
     *     name: get one user
     *     summary: get one user
     *     parameters:
     *        - in: path
     *          name: id
     *          type: string
     *          required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let user = await User.findById(id).select('-__v -password');
            body = {user};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User details',
                message: e.message || 'An error is occured into user details',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Create one user
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /user:
     *   post:
     *     tags:
     *       - User
     *     name: Create one user
     *     summary: Create one user
     *     parameters:
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async store(req, res){
        let status = 200;
        let body = {};

        try{
           console.log(req.body.password);
           req.body.password = Auth.encrypt(req.body.password);
            let user = await User.create(req.body);
            body={user};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User create',
                message: e.message || 'An error is occured into user create',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Update one user
     * @param req
     * @param res
     * @returns {Promise<any>}
     * 
     * @swagger
     * /user/{id}:
     *   put:
     *     tags:
     *       - User
     *     name: Update one user
     *     summary: Update one user
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let user = await User.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v -password');
            body = {user};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User update',
                message: e.message || 'An error is occured into user update',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Remove one user
     * @param req
     * @param res
     * @returns {Promise<any>}
     * 
     * @swagger
     * 
     * /user/{id}:
     *   delete:
     *     tags:
     *       - User
     *     name: Delete one user
     *     summary: Delete one user
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
          let user=  await User.findByIdAndDelete(id);
          if(fs.existsSync(`./${user.picture}`)){
            await fs.unlinkSync(`./${user.picture}`);
        }
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User remove',
                message: e.message || 'An error is occured into user remove',
            }
        }
        return res.status(status).json(body);
    }
    static async updatePicture(req, res){
        let status = 200;
        let body = {};

        try{
            let user = await User.findById(req.params.id);
            if(fs.existsSync(`./${user.picture}`)){
                await fs.unlinkSync(`./${user.picture}`);
            }
            user.picture = req.body.picture;
            await user.save();
            body = {user};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Update video',
                message: e.message || 'An error is occured into video updated',
            }
        }
        return res.status(status).json(body);
    }

}
