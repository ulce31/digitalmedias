import Music from "../models/Music";

export default class MusicController{

    /**
     * Get list of Musics
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /musics:
     *   get:
     *     tags:
     *       - Musics
     *     name: List of musics
     *     summary: List of musics
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     * 
     */
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let music = await Music.find().select('-__v');
            body = {music};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'musics details',
                message: e.message || 'An error is occured into musics details',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Create one Music
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /musics:
     *   post:
     *     tags:
     *       - Musics
     *     name: Create one musics
     *     summary: Create one musics
     *     parameters:
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async store(req, res){
        let status = 200;
        let body = {};

        try{
            let music = await Music.create(req.body);
            body = {music};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'musics details',
                message: e.message || 'An error is occured into musics details',
            }
        }
        return res.status(status).json(body);
    }

       /**
     * Get details of Music
     * @param req
     * @param res
     * @returns {Promise<any>}
     *@swagger
     * /musics/{id}:
     *   get:
     *     tags:
     *       - Musics
     *     name: get one musics
     *     summary: get one musics
     *     parameters:
     *        - in: path
     *          name: id
     *          type: string
     *          required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let music = await Music.findById(id).select('-__v');
            body = {music};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Document details',
                message: e.message || 'An error is occured into Document details',
            }
        }
        return res.status(status).json(body);
    }
    /**
     * Update one Music
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /musics/{id}:
     *   put:
     *     tags:
     *       - Musics
     *     name: Update one musics
     *     summary: Update one musics
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let music = await Music.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {music};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'musics details',
                message: e.message || 'An error is occured into musics details',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Remove one Music
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * 
     * /musics/{id}:
     *   delete:
     *     tags:
     *       - Musics
     *     name: Delete one musics
     *     summary: Delete one musics
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Music.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'musics details',
                message: e.message || 'An error is occured into musics details',
            }
        }
        return res.status(status).json(body);
    }
}
