import Book from "../models/Book";
import fs from 'fs';
export default class BookController{

    /**
     * Get list of Books
     * @param req
     * @param res
     * @returns {Promise<any>}
     *@swagger
     * /books:
     *   get:
     *     tags:
     *       - Books
     *     name: List of books
     *     summary: List of books
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let book = await Book.find().select('-__v');
            body = {book};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User details',
                message: e.message || 'An error is occured into user details',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * 
     * @param req
     * @param res
     * @returns {Promise<any>}
     * 
     * @swagger
     * /books:
     *   post:
     *     tags:
     *       - Books
     *     name: Create one books
     *     summary: Create one books
     *     parameters:
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */         
     
    static async store(req, res){
        let status = 200;
        let body = {};

        try{
            let book = await Book.create(req.body);
            body = {book};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User details',
                message: e.message || 'An error is occured into user details',
            }
        }
        return res.status(status).json(body);
    }

       /**
     * Get details of Book
     * @param req
     * @param res
     * @returns {Promise<any>}
     *@swagger
     * /books/{id}:
     *   get:
     *     tags:
     *       - Books
     *     name: get one books
     *     summary: get one books
     *     parameters:
     *        - in: path
     *          name: id
     *          type: string
     *          required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let book = await Book.findById(id).select('-__v');
            body = {book};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Document details',
                message: e.message || 'An error is occured into Document details',
            }
        }
        return res.status(status).json(body);
    }
    /**
     * Update one Book
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /books/{id}:
     *   put:
     *     tags:
     *       - Books
     *     name: Update one books
     *     summary: Update one books
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let book = await Book.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {book};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User details',
                message: e.message || 'An error is occured into user details',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Remove one Book
     * @param req
     * @param res
     * @returns {Promise<any>}
     * 
     * @swagger
     * /books/{id}:
     *   delete:
     *     tags:
     *       - Books
     *     name: Delete one books
     *     summary: Delete one books
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Book.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User details',
                message: e.message || 'An error is occured into user details',
            }
        }
        return res.status(status).json(body);
    }
}
