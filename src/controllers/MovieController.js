import Movie from "../models/Movie";

export default class MovieController{

    /**
     * Get list of Movies
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /movies:
     *   get:
     *     tags:
     *       - Movies
     *     name: List of movies
     *     summary: List of movies
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     * 
     */
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let movie = await Movie.find().select('-__v');
            body = {movie};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User details',
                message: e.message || 'An error is occured into user details',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Create one Movie
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /movies:
     *   post:
     *     tags:
     *       - Movies
     *     name: Create one movies
     *     summary: Create one movies
     *     parameters:
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async store(req, res){
        let status = 200;
        let body = {};

        try{
            let movie = await Movie.create(req.body);
            body = {movie};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User details',
                message: e.message || 'An error is occured into user details',
            }
        }
        return res.status(status).json(body);
    }

       /**
     * Get details of Movie
     * @param req
     * @param res
     * @returns {Promise<any>}
     *@swagger
     * /movies/{id}:
     *   get:
     *     tags:
     *       - Movies
     *     name: get one movies
     *     summary: get one movies
     *     parameters:
     *        - in: path
     *          name: id
     *          type: string
     *          required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let movie = await Movie.findById(id).select('-__v');
            body = {movie};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Document details',
                message: e.message || 'An error is occured into Document details',
            }
        }
        return res.status(status).json(body);
    }
    /**
     * Update one Movie
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /movies/{id}:
     *   put:
     *     tags:
     *       - Movies
     *     name: Update one movies
     *     summary: Update one movies
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let movie = await Movie.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {movie};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User details',
                message: e.message || 'An error is occured into user details',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Remove one Movie
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * 
     * /movies/{id}:
     *   delete:
     *     tags:
     *       - Movies
     *     name: Delete one movies
     *     summary: Delete one movies
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Movie.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User details',
                message: e.message || 'An error is occured into user details',
            }
        }
        return res.status(status).json(body);
    }
}
