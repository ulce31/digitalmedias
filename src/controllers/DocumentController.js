import Document from "../models/Document";
import fs from 'fs';
/*import Utils from "../utils/Utils";*/

export default class DocumentController{


    /**
     * Get list of Documents
     * @param req
     * @param res
     * @returns {Promise<any>}
     * 
     *@swagger
     * /documents:
     *   get:
     *     tags:
     *       - Documents
     *     name: List of documents
     *     summary: List of documents
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            //Récupération utilisateurs
            let Documents = await Document.find().select('-__v').populate('genre');
            body = {Documents};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Document list',
                message: e.message || 'An error is occured into Document list',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Get details of Document
     * @param req
     * @param res
     * @returns {Promise<any>}
     * 
     *@swagger
     * /documents/{id}:
     *   get:
     *     tags:
     *       - Documents
     *     name: get one documents
     *     summary: get one documents
     *     parameters:
     *        - in: path
     *          name: id
     *          type: string
     *          required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
<<<<<<< HEAD
            let document = await Document.findById(id).populate('genre');
=======
            let document = await Document.findById(id).select('-__v').populate('genre');
            console.log(document.genres);
>>>>>>> 8633fa48435c989eebe1db6d6be94d21509a9495
            body = {document};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Document details',
                message: e.message || 'An error is occured into Document details',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Create one Document
     * @param req
     * @param res
     * @returns {Promise<any>}
     * 
     * @swagger
     * /documents:
     *   post:
     *     tags:
     *       - Documents
     *     name: Create one documents
     *     summary: Create one documents
     *     parameters:
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async store(req, res){
        let status = 200;
        let body = {};
    

        try{
            let document = await Document.create(req.body);
            body={document};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Document create',
                message: e.message || 'An error is occured into Document create',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Update one Document
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * /documents/{id}:
     *   put:
     *     tags:
     *       - Documents
     *     name: Update one documents
     *     summary: Update one documents
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *       - name: body
     *         in: body
     *         type: json
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let document = await Document.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {document};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Document update',
                message: e.message || 'An error is occured into Document update',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Remove one Document
     * @param req
     * @param res
     * @returns {Promise<any>}
     * @swagger
     * 
     * /documents/{id}:
     *   delete:
     *     tags:
     *       - Documents
     *     name: Delete one documents
     *     summary: Delete one documents
     *     parameters:
     *       - name: id
     *         in: path
     *         type: string
     *         required: true
     *     consumes:
     *        - application/ json
     *     responses:
     *        200:
     *          description: Data was found and display it
     */
    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
           let document=  await Document.findByIdAndDelete(id);
            if(fs.existsSync(`./${document.picture}`)){
                await fs.unlinkSync(`./${document.picture}`);
            }

        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Document remove',
                message: e.message || 'An error is occured into Document remove',
            }
        }
        return res.status(status).json(body);
    }
    static async updatePicture(req, res){
        let status = 200;
        let body = {};

        try{
            let document = await Document.findById(req.params.id);
            if(fs.existsSync(`./${document.picture}`)){
                await fs.unlinkSync(`./${document.picture}`);
            }
            document.picture = req.body.picture;
            await document.save();
            body = {document};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Update video',
                message: e.message || 'An error is occured into video updated',
            }
        }
        return res.status(status).json(body);
    }

}
