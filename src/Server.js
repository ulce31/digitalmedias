import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import router from "./routes";
// import jwt from "./config/jwt";
import swaggerUi from 'swagger-ui-express';
import swaggerJsDoc from 'swagger-jsdoc';
// import yamljs from 'yamljs';
// const swaggerDoc = yamljs.load('swagger.yaml');


export default class Server{
    /**
     * Config server
     * @returns {app}
     * 
     */
    static config(){
        const app= express();
       // app.use(jwt());
        const swaggerDoc = swaggerJsDoc({
            swaggerDefinition: {
                info: {
                    title:"DegitalMedia",
                    version: '1.0.0',
                    description: 'DegitalMedia API',
                },
                definitions:{
                    Genre:{
                        type: "object",
                        properties:{
                            libelle:{
                                type: "string"
                            }
                        } 
                    },
                    Book:{
                        type: "object",
                        properties:{
                            libelle:{
                                type: "string"
                            }
                        } 
                    },
                    Borrow:{
                        type: "object",
                        properties:{
                            libelle:{
                                type: "string"
                            }
                        } 
                    },
                    Comment:{
                        type: "object",
                        properties:{
                            libelle:{
                                type: "string"
                            }
                        } 
                    },
                    Document:{
                        type: "object",
                        properties:{
                            libelle:{
                                type: "string"
                            }
                        } 
                    },
                    Movie:{
                        type: "object",
                        properties:{
                            libelle:{
                                type: "string"
                            }
                        } 
                    },
                    Music:{
                        type: "object",
                        properties:{
                            libelle:{
                                type: "string"
                            }
                        } 
                    },
                    User:{
                        type: "object",
                        properties:{
                            libelle:{
                                type: "string"
                            }
                        } 
                    },

                }
         
            },
            apis: ["src/controllers/*.js"]
        });
        app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));
        //Configuration
        app.use(bodyParser.urlencoded({extended:false}));//encoder url avec json 
        app.use(bodyParser.json());//
        app.use(cors({origin:true}));//accepter les requetes d'autres serveur
    //Configuration des routes 
        app.use('/',router);
        return app;
    }
}
